.. Profile Likelihood documentation master file, created by
   sphinx-quickstart on Mon Feb  1 13:21:19 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Profile Likelihood
==================

``Profile_likelihood`` is a collection of tools to compute and process
the profile likelihood of a model.

.. toctree::
   :maxdepth: 2
   :caption: Basics:

   get_started/get_started
   theory
   modules/modules

.. toctree::
   :maxdepth: 2
   :caption: How to:

   howto/howto

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
