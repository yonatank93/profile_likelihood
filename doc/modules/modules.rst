.. _modules:

=======
Modules
=======

.. toctree::
   :maxdepth: 2

   profile_likelihood
   default_classes
