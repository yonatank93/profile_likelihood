.. _profile_likelihoo:

===================
profile\_likelihood
===================

profile\_likelihood.profile\_likelihood
----------------------------------------------

.. automodule:: profile_likelihood

   .. autoclass:: profile_likelihood
      :members:
	 .. attribute:: model
	 .. attribute:: nparams
	 .. attribute:: npred
	 .. attribute:: param_names
	 .. attribute:: results
	 .. attribute:: best_results
	 .. method:: compute
	 .. method:: save_results
	 .. method:: save_best_results
	 .. method:: load_results
	 .. method:: plot_likelihoods
	 .. method:: plot_paths
	 .. method:: plot_likelihoods_and_paths
      :undoc-members:
      :show-inheritance:
      :inherited-members:
