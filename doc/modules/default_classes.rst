.. _default_classes:

===============
Default classes
===============

single\_starting\_point
-----------------------------------------------

.. automodule:: profile_likelihood.default.single_starting_point
   :members:

fit\_leastsq
-----------------------------------------------

.. automodule:: profile_likelihood.default.fit_leastsq
   :members:
