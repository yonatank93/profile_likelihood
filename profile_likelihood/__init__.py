"""Tools to compute and present the profile likelihood of a model."""

from .profile_likelihood import profile_likelihood

__all__ = ["profile_likelihood"]
__version__ = "0.6.0"
