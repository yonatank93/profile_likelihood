List of (incomplete) examples about using `profile_likelihood`:

1. [Basic computation](https://git.physics.byu.edu/yonatank/profile_likelihood/blob/add-doc/examples/basic_computation.ipynb)
2. [Geometric representation](https://git.physics.byu.edu/yonatank/profile_likelihood/blob/add-doc/examples/geometric_representation.ipynb)
3. [Parallel computing](https://git.physics.byu.edu/yonatank/profile_likelihood/blob/add-doc/examples/parallel_computing.ipynb)
4. [Custom fixed points](https://git.physics.byu.edu/yonatank/profile_likelihood/blob/add-doc/examples/custom_fixed_points.ipynb)
5. [Multiple starting points](https://git.physics.byu.edu/yonatank/profile_likelihood/blob/add-doc/examples/multiple_starting_points.ipynb)
6. [Custom fitting routine](https://git.physics.byu.edu/yonatank/profile_likelihood/blob/add-doc/examples/custom_fitting.ipynb)
7. [Save and load results](https://git.physics.byu.edu/yonatank/profile_likelihood/blob/add-doc/examples/save_and_load.ipynb)
8. [Plotting routines](https://git.physics.byu.edu/yonatank/profile_likelihood/blob/add-doc/examples/plotting_routines.ipynb)
